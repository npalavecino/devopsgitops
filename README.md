**Despliegue app  local Kubernetes** 


- Configuración de ambiente

- [ ] Preparación de ambiente local - Minikube (instalar si aplica)

[URL instalación github - minikube](https://github.com/kubernetes/minikube)

_Minikube es una distribución reducidade Kubernetes que permite alcanzar el máximo rendimiento funcional de esta herramienta con el mínimo esfuerzo. Esto no solo es interesante para quienes se están iniciando en el uso de contenedores, sino también, y sobre todo, en el mundo del edge computing y el del Internet de las cosas. _


- Comandos Basicos

```sh
 $ minikube start
 $ minikube status 
 $ minikube stop
 $ minikube ip
 $ minikube dashboard
 $ minikube delete
```
- Ejemplo

``` sh
 $ kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.10
 $ kubectl expose deployment hello-minikube --type=NodePort --port=8080
 $ kubectl get pod
 $ minikube service hello-minikube --url
 $ kubectl get service
 $ minikube dashboard
 $ kubectl delete services hello-minikube
 $ kubectl delete deployment hello-minikube
```

Se sigue probando vscode para trabajo diario.

